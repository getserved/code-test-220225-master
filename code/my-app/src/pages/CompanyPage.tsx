import React from 'react'
import Company from "../components/Company/Company"
import Layout from "../components/Layout/Layout"

function CompanyPage() {
  return (
    <Layout>
        <Company />
    </Layout>
  );
}

export default CompanyPage;
