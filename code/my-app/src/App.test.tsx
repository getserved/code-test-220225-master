import React from 'react';
import App from './App';
import { renderWithProviders } from './utils/TestUtils'
import { MemoryRouter } from 'react-router-dom';

test('renders redirected link', async () => {
  renderWithProviders(
    <MemoryRouter initialEntries={['/']}>
      <App />    
    </MemoryRouter>
  )
  await setTimeout(() => expect(window.location.pathname).toEqual('/company/1'));
});
