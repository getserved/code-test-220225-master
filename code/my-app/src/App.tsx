import React, { Route, Routes, Navigate } from "react-router-dom";
import CompanyPage from "./pages/CompanyPage";
import Company from "./components/Company/Company";

function App() {
  return (
    <Routes>
      <Route path='/' element={<Navigate replace to="/company/1" />}/>
      <Route path='/company' element={<CompanyPage />}>
        <Route path=":page" element={<Company />} />
      </Route>
    </Routes>
  );
}

export default App;
