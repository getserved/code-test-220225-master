import React, { PropsWithChildren } from 'react'
import { render } from '@testing-library/react'
import type { RenderOptions } from '@testing-library/react'
import type { PreloadedState } from '@reduxjs/toolkit'
import { Provider } from 'react-redux'
import { setupStore } from '../core/reducers'
import type { AppStore, RootState } from '../core/reducers'

// This type interface extends the default options for render from RTL, as well
// as allows the user to specify other things such as initialState, store.
interface ExtendedRenderOptions extends Omit<RenderOptions, 'queries'> {
    preloadedState?: PreloadedState<RootState>
    store?: AppStore
  }

export const employees = [
    {
        id: 'id',
        avatar: 'avatar',
        firstName: 'firstName',
        lastName: 'lastName',
        fullName: 'firstName lastName',
        jobTitle: 'jobTitle', 
        contactNo: 'contactNo',
        address: 'address',
        age: 18,
        bio: 'bio',
        dateJoined: '01/01/1970'
    },
    {
        id: 'id2',
        avatar: 'avatar2',
        firstName: 'firstName2',
        lastName: 'lastName2',
        fullName: 'firstName2 lastName2',
        jobTitle: 'jobTitle2', 
        contactNo: 'contactNo2',
        address: 'address2',
        age: 28,
        bio: 'bio2',
        dateJoined: '02/02/1970'
      }
]

export const companyInfo = {
    companyName: 'companyName',
    companyMotto: 'companyMotto',
    companyEst: '01/01/1970'
}

export const company = {
    companyInfo,
    employees
}

export const companyRoot = {
    company: company,
    filteredEmployees: [],
    selectedEmployeeId: '',
    selectedEmployee: undefined,
    query: '',
    sortBy: '',
    sortDescending: false,
    isLoading: true,
    errors: undefined
}

export function renderWithProviders(
    ui: React.ReactElement,
    {
      preloadedState ={
        company:companyRoot
      },
      // Automatically create a store instance if no store was passed in
      store = setupStore(preloadedState),
      ...renderOptions
    }: ExtendedRenderOptions = {}
  ) {
    // eslint-disable-next-line @typescript-eslint/ban-types
    function Wrapper({ children }: PropsWithChildren<{}>): JSX.Element {
      return <Provider store={store}>{children}</Provider>
    }
    return { store, ...render(ui, { wrapper: Wrapper, ...renderOptions }) }
  }