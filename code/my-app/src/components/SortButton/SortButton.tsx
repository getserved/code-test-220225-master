import React, { FC } from "react";

import $ from "./SortButton.module.css";
import Button, {ButtonType, VariantType} from "../Button/Button"
import _ from "lodash"

interface SortButtonProps {
  className?:string
  disabled?: boolean,
  onSortAscending?: () => void
  onSortDescending?: () => void
}

const SortButton: FC<SortButtonProps> = ({
  className,
  disabled = false,
  onSortAscending,
  onSortDescending
}) => {

  const throttleOnSortDesending = onSortDescending?_.throttle(onSortDescending, 500): undefined
  const throttleOnSortAscending = onSortAscending?_.throttle(onSortAscending, 500): undefined

  const handleClickOnSortDescending = () => {
    throttleOnSortDesending && throttleOnSortDesending()
  }

  const handleClickOnSortAscending = () => {
    throttleOnSortAscending && throttleOnSortAscending()
  }

  return (
    <div className={`${$.container} ${className?className:''}`}>
      <div className={$.buttonWrapper}>
        <Button 
          tabIndex={0}
          disabled={disabled}
          className={$.buttonUp}
          type={ButtonType.BUTTON}
          variant={VariantType.OUTLINE}
          onClick={handleClickOnSortAscending}
          data-testid="sort-ascending"
        >
          <span className={$.triangleUp} data-testid="icon-ascending"></span>
        </Button>
        <Button 
          tabIndex={0}
          disabled={disabled}
          className={$.buttonDown}
          type={ButtonType.BUTTON}
          variant={VariantType.OUTLINE}
          onClick={handleClickOnSortDescending}
          data-testid="sort-descending"
        >
          <span className={$.triangleDown} data-testid="icon-descending"></span>
        </Button>
      </div>
    </div>
  );
};

export default SortButton;
