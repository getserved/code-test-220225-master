import { fireEvent, screen, render } from '@testing-library/react'
import '@testing-library/jest-dom'
import SortButton from './SortButton'

const props = {
  
}

test('SortButton rendering with init', () => {
  render(<SortButton />)
  const up = screen.getByTestId('sort-ascending')
  const down = screen.getByTestId('sort-descending')
  expect(up).toBeInTheDocument()
  expect(down).toBeInTheDocument()
  expect(screen.getByTestId('icon-ascending')).toHaveClass('triangleUp')
  expect(screen.getByTestId('icon-descending')).toHaveClass('triangleDown')
})

test('SortButton click event', () => {
  const mockAsendingOnClick = jest.fn();
  const mockDesendingOnClick = jest.fn();
  render(<SortButton onSortAscending={mockAsendingOnClick} onSortDescending={mockDesendingOnClick}/>)
  const up = screen.getByTestId('sort-ascending')
  const down = screen.getByTestId('sort-descending')

  fireEvent.click(up)
  expect(mockAsendingOnClick).toBeCalledTimes(1)

  fireEvent.click(down)
  expect(mockDesendingOnClick).toBeCalledTimes(1)
})

test('SortButton click event will not trigger with disabled', () => {
  const mockAsendingOnClick = jest.fn();
  const mockDesendingOnClick = jest.fn();
  render(<SortButton disabled onSortAscending={mockAsendingOnClick} onSortDescending={mockDesendingOnClick}/>)
  const up = screen.getByTestId('sort-ascending')
  const down = screen.getByTestId('sort-descending')

  fireEvent.click(up)
  expect(mockAsendingOnClick).toBeCalledTimes(0)

  fireEvent.click(down)
  expect(mockDesendingOnClick).toBeCalledTimes(0)
})