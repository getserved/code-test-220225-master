import { screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import Header from './Header'
import { renderWithProviders } from '../../utils/TestUtils'

describe('Header component', () => {
  it('renders the header', () => {
    renderWithProviders(
        <Header />
    );
    expect(screen.getByTestId('header-titles')).toBeInTheDocument()
    expect(screen.getByTestId('header-title')).toBeInTheDocument()
    expect(screen.getByTestId('header-subtitle')).toBeInTheDocument()
    expect(screen.getByTestId('header-motto')).toBeInTheDocument()
    expect(screen.getByTestId('header-est')).toBeInTheDocument()
    expect(screen.getByTestId('header-title').textContent).toBe('companyName')
    expect(screen.getByTestId('header-motto').textContent).toBe('companyMotto')
    expect(screen.getByTestId('header-est').textContent).toBe('Since 01/01/1970')
  })
});
