import React, { FC, useMemo } from "react";
import { useAppSelector } from "../hooks"
import $ from "./Header.module.css";
import moment from "moment"

const Header: FC = () => {

  const { companyName, companyMotto, companyEst} = useAppSelector(state => state.company.company.companyInfo)

  const formattedEst = useMemo(() => {
    return moment(companyEst).format('DD/MM/YYYY')
  }, [companyEst])

  return (
    <header className={$.header}>
        <div className={$.titles} data-testid="header-titles">
          <h2 className={$.title} data-testid="header-title">
            {companyName}
          </h2>
          <div className={$.subtitle} data-testid="header-subtitle">
            <span data-testid="header-motto">{companyMotto}</span>
          </div>
        </div>
        <div className={$.since} data-testid="header-est">
          Since {formattedEst}
        </div>
    </header>
  );
};

export default Header;
