import React from 'react'
import { screen } from '@testing-library/react'
import '@testing-library/jest-dom';
import Layout from './Layout'
import { renderWithProviders } from '../../utils/TestUtils'

describe('Layout component', () => {
  it('renders the title', () => {
    renderWithProviders(
      <Layout>test</Layout>
    );
    expect(screen.getByText('test')).toBeInTheDocument()
  });

});