import { useDispatch } from "react-redux";
import { AppDispatch } from "../../core/reducers"
import { fetchCompany, sort, setSort, setQuery, filter, setSelectedEmployeeId } from "../../core/reducers/CompanySlice"

export default function useCompany() {
  const dispatch = useDispatch<AppDispatch>();

  return {
    // fetch company by action
    fetchCompany: () => {
      dispatch(fetchCompany())
    },

    // sort employees
    sort: () => {
      dispatch(sort({}))
    },

    // filter employees
    filter: () => {
      dispatch(filter({}))
    },

    // set sortBy and sortDescending for sorting
    setSort: (field:string, descending:boolean) => {
      dispatch(setSort({field, descending}))
    },

    // set query for filtering
    setQuery: (query:string) => {
      dispatch(setQuery({query}))
    },

    // set selectedId and selectedEmployee
    setSelectedEmployeeId: (id: string) => {
      dispatch(setSelectedEmployeeId(id))
    }
  }
}
