import React, { FC, useEffect, useMemo } from "react";
import { useAppSelector } from "../hooks";
import type { Employee } from "../../core/types"
import Pagination from "../Pagination/Pagination";
import $ from "./DataTable.module.css";
import SortButton from "../SortButton/SortButton"
import useCompany from "../hooks/useCompany"
import LazyImage from "../LazyImage/LazyImage"

interface DataTableProps extends React.ButtonHTMLAttributes<HTMLDivElement>{
  currentPage?: number
}

const DataTable: FC<DataTableProps> = ({
  currentPage = 1
}) => {

  const numberPerPage = 3

  const { filteredEmployees: employees, sortBy, sortDescending, query } = useAppSelector(state => state.company)

  const { sort, setSort, filter, setSelectedEmployeeId } = useCompany()

  const headers = [
    {
      name: "ID",
      sortBy: 'id',
      sortable: false
    },
    {
      name: "Name",
      sortBy: 'fullName',
      sortable: true
    },
    {
      name: "Contact",
      sortBy: 'contactNo',
      sortable: true
    },
    {
      name: "Address",
      sortBy: 'address',
      sortable: true
    },
  ]

  useEffect(() => {
    sort()
  }, [sortBy, sortDescending])

  useEffect(() => {
    filter()
  }, [query])
  
  const totalNumber = useMemo(()=>{
    return employees.length
  }, [employees]) 

  const filteredList = useMemo(() => {
    return (currentPage && currentPage > 0)?employees.slice((currentPage - 1) * numberPerPage, currentPage * numberPerPage) : []
  }, [employees, currentPage])

  const handleSort = (field:string, descending: boolean) => {
    setSort(field, descending)
  }

  const handleSelectEmployee = (id: string) => {
    setSelectedEmployeeId(id)
  }

  return (
    <div className={$.container}>
        <div className={$.statistics}></div>
        <div className={$.table}>
            <div className={$.theader}>
              <div className={$.tr}>
                {headers && headers.map((header,k) => {
                  return (
                    <div key={`th_${k}`} className={`${$.th} ${header.sortable?$.sortable:''} ${header.name === 'Name'? $.avatarWrapper: ''}`}>
                      <span className={header.sortable?$.sortName:''} data-testid={`table-header-${header.name}`}>{header.name}</span>
                      {header.sortable && <SortButton 
                        className={$.sortButton}
                        onSortAscending={() => handleSort(header.sortBy, false)}
                        onSortDescending={() => handleSort(header.sortBy, true)}
                      />}
                  </div>
                  )
                })}
              </div>
            </div>
            <div className={$.tbody}>
              {filteredList && filteredList.map((employee: Employee, k) => {
                return (
                  <div key={`employee_${employee.id}`} className={$.tr} onClick={() => handleSelectEmployee(employee.id)}>
                    <div className={`${$.td} ${$.id}`} data-testid={`table-col-${k}-id`}>{employee.id}</div>
                    <div className={`${$.td} ${$.avatarWrapper}`} data-testid={`table-col-${k}-name`}>
                      <div className={$.avatar} data-testid={`table-col-${k}-avatar`}><LazyImage alt="" src={employee.avatar}/></div>
                      {employee.fullName}
                    </div>
                    <div className={$.td} data-testid={`table-col-${k}-contact`}>{employee.contactNo}</div>
                    <div className={$.td} data-testid={`table-col-${k}-address`}>{employee.address}</div>
                  </div>
                )
              })}
            </div>
        </div>
        <div className={$.pagination}>
          <Pagination totalNumber={totalNumber} numberPerPage={numberPerPage} currentPage={currentPage}/>
        </div>
    </div>
  );
};

export default DataTable;
