import React from 'react'
import { screen, waitFor } from '@testing-library/react'
import '@testing-library/jest-dom'
import { MemoryRouter } from 'react-router-dom';
import DataTable from './DataTable'
import { renderWithProviders } from '../../utils/TestUtils'
import { setQuery, filter } from "../../core/reducers/CompanySlice"
import { act } from 'react-dom/test-utils';


test('DataTable rendering with init', () => {
  renderWithProviders(
    <MemoryRouter initialEntries={['/company/1']}>
      <DataTable />    
    </MemoryRouter>
  )
  expect(screen.getByTestId('table-header-ID')).toBeInTheDocument()
  expect(screen.getByTestId('table-header-Name')).toBeInTheDocument()
  expect(screen.getByTestId('table-header-Contact')).toBeInTheDocument()
  expect(screen.getByTestId('table-header-Address')).toBeInTheDocument()

  expect(screen.getByTestId('table-col-0-id')).toBeInTheDocument()
  expect(screen.getByTestId('table-col-0-name')).toBeInTheDocument()
  expect(screen.getByTestId('table-col-0-avatar')).toBeInTheDocument()
  expect(screen.getByTestId('table-col-0-contact')).toBeInTheDocument()
  expect(screen.getByTestId('table-col-0-address')).toBeInTheDocument()

  expect(screen.getByTestId('table-col-0-id').textContent).toBe('id')
  expect(screen.getByTestId('table-col-0-name').textContent).toBe('firstName lastName')
  expect(screen.getByTestId('table-col-0-contact').textContent).toBe('contactNo')
  expect(screen.getByTestId('table-col-0-address').textContent).toBe('address')
})

test('DataTable rerendering when filteredEmployees updates', async () => {
  const { store } = renderWithProviders(
    <MemoryRouter initialEntries={['/company/1']}>
      <DataTable />    
    </MemoryRouter>
  )
  
  expect(screen.getByTestId('table-col-0-id').textContent).toBe('id')
  expect(screen.getByTestId('table-col-0-name').textContent).toBe('firstName lastName')
  expect(screen.getByTestId('table-col-0-contact').textContent).toBe('contactNo')
  expect(screen.getByTestId('table-col-0-address').textContent).toBe('address')

  await act(()=> {
    store.dispatch(setQuery({query: 'id2'}))
  })
  
  
  await waitFor(() => {
    const state = store.getState();
    expect(state.company.query).toBe('id2')
  })

  await act(()=> {
    store.dispatch(filter({}))
  })
  
  await waitFor(() => {
    const state = store.getState();
    expect(state.company.filteredEmployees.length).toBe(1)
  })

  
  expect(screen.getByTestId('table-col-0-id').textContent).toBe('id2')
  expect(screen.getByTestId('table-col-0-name').textContent).toBe('firstName2 lastName2')
  expect(screen.getByTestId('table-col-0-contact').textContent).toBe('contactNo2')
  expect(screen.getByTestId('table-col-0-address').textContent).toBe('address2')

})
