import { fireEvent, screen, render } from '@testing-library/react'
import '@testing-library/jest-dom'
import LazyImage from './LazyImage'

const props = {
  alt:"test",
  src:"https://s3.amazonaws.com/uifaces/faces/twitter/findingjenny/128.jpg",
}

describe('LazyImage component', () => {
  it('renders the LazyImage with init state', () => {
    const {container} = render(
     <LazyImage {...props} /> 
    );
    expect(container).toBeInTheDocument()
    const img = screen.getByTestId('lazy-img')
    expect(img).toBeInTheDocument()
    expect(screen.queryByTestId('error-img')).not.toBeInTheDocument()
    expect(img.getAttribute('src')).toBe("https://s3.amazonaws.com/uifaces/faces/twitter/findingjenny/128.jpg")
    expect(img.getAttribute('alt')).toBe("test")

    fireEvent.error(img);

    expect(screen.queryByTestId('lazy-img')).not.toBeInTheDocument()
    expect(screen.getByTestId('error-img')).toBeInTheDocument()
  });
});
