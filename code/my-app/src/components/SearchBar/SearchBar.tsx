import React, { FC, useState, useRef } from "react";
import $ from "./SearchBar.module.css"
import Button from "../Button/Button"
import useCompany from "../hooks/useCompany"
import _ from "lodash"

const SearchBar: FC = () => {

    const [queryStr, setQueryStr] = useState<string>('')

    const searchBtnRef = useRef<HTMLButtonElement>(null)

    const { setQuery } = useCompany()

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setQueryStr(event.target.value)
    }

    const handleInputKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        switch (event.key) {
            case 'Enter':
                if (searchBtnRef.current) searchBtnRef.current.click()
                break;
        }
    }

    const handleBtnKeyDown = (event: React.KeyboardEvent<HTMLButtonElement>) => {
        switch (event.key) {
            case 'Enter':
                if (searchBtnRef.current) throttleSearch()
                break;
        }
    }
   
    const search = () => {
        setQuery(queryStr)
    }

    const throttleSearch = _.throttle(search, 1500)

    return (
        <div className={$.container}>
            <input className={$.query} onChange={handleChange} onKeyDown={handleInputKeyDown} type="text" value={queryStr} data-testid='search-input'/>
            <Button ref={searchBtnRef} className={$.search} onClick={() => throttleSearch()} onKeyDown={handleBtnKeyDown} data-testid='search-btn'>Search</Button>
        </div>
    );
};

export default SearchBar;
