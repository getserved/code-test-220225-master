import { fireEvent, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import SearchBar from './SearchBar'
import { renderWithProviders } from '../../utils/TestUtils'

test('SearchBar rendering with init', () => {
  renderWithProviders(<SearchBar />)
  const input = screen.getByTestId('search-input')
  const btn = screen.getByTestId('search-btn')
  expect(input).toBeInTheDocument()
  expect(btn).toBeInTheDocument()
  expect(btn).toHaveTextContent('Search')
})

test('SearchBar when inputing', async () => {
  const {store} = renderWithProviders(<SearchBar />)
  const state = store.getState()
  const input = screen.getByTestId('search-input')
  expect(input).toBeInTheDocument()
  
  fireEvent.change(input, { target: { value: 'test' } });

  await setTimeout(() => {
    expect(state.company.query).toBe('test')
  }, 0)
})

test('SearchBar when searching with failed query', async () => {
  const {store} = renderWithProviders(<SearchBar />)
  const state = store.getState()
  const input = screen.getByTestId('search-input')
  const btn = screen.getByTestId('search-btn')
  expect(input).toBeInTheDocument()
  
  fireEvent.change(input, { target: { value: 'test' } });

  await setTimeout(() => {
    expect(state.company.query).toBe('test')
  }, 0)

  fireEvent.click(btn)

  await setTimeout(() => {
    expect(state.company.filteredEmployees).toBe([])
  }, 0)
})

test('SearchBar when searching with successful query', async () => {
  const {store} = renderWithProviders(<SearchBar />)
  const state = store.getState()
  const input = screen.getByTestId('search-input')
  const btn = screen.getByTestId('search-btn')
  expect(input).toBeInTheDocument()
  
  fireEvent.change(input, { target: { value: 'id' } });

  await setTimeout(() => {
    expect(state.company.query).toBe('id')
  }, 0)

  fireEvent.click(btn)

  await setTimeout(() => {
    expect(state.company.filteredEmployees.length).toBe(1)
  }, 0)
})

test('SearchBar when keyPress ”Enter"', async () => {
  const {store} = renderWithProviders(<SearchBar />)
  const state = store.getState()
  const input = screen.getByTestId('search-input')
  expect(input).toBeInTheDocument()
  
  fireEvent.focus(input);
  fireEvent.change(input, { target: { value: 'id' } });
  fireEvent.keyPress(input, { key: 'Enter', code: 13, charCode: 13 });

  await setTimeout(() => {
    expect(state.company.query).toBe('id')
  }, 0)

  await setTimeout(() => {
    expect(state.company.filteredEmployees.length).toBe(1)
  }, 0)
})