import React from "react";
import $ from "./Modal.module.css";
import Button, {VariantType} from "../Button/Button"

interface ModalProps {
  children: React.ReactNode,
  isOpen?: boolean,
  onClose?: () => void;
}

const Modal: React.FC<ModalProps> = ({ 
  children,
  isOpen = false,
  onClose
 }) => {
  const handleClose = () => {
    isOpen = false
    onClose && onClose();
  };

  return (
    <>
      {isOpen && (
        <div className={$.overlay} data-testid="modal-overlay">
          <div className={$.modal} data-testid="modal">
            <Button className={$.close} variant={VariantType.OUTLINE} onClick={handleClose} data-testid="modal-close">
              &times;
            </Button>
            {children}
          </div>
        </div>
      )}
    </>
  );
};

export default Modal;