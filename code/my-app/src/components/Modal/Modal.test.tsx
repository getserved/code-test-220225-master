import { fireEvent, screen, render } from '@testing-library/react'
import '@testing-library/jest-dom'
import Modal from './Modal'

test('Modal rendering with closed status ', () => {
  render(<Modal>Test</Modal>)
  expect(screen.queryByTestId('modal-overlay')).not.toBeInTheDocument()
  expect(screen.queryByTestId('modal')).not.toBeInTheDocument()
  expect(screen.queryByTestId('modal-close')).not.toBeInTheDocument()
})

test('SortButton rendering with open status ', () => {
  render(<Modal isOpen>Test</Modal>)
  expect(screen.getByTestId('modal-overlay')).toBeInTheDocument()
  expect(screen.getByTestId('modal')).toBeInTheDocument()
  expect(screen.getByTestId('modal-close')).toBeInTheDocument()
})

test('SortButton rendering with open status and click close ', async () => {
  render(<Modal isOpen>Test</Modal>)
  expect(screen.getByTestId('modal-overlay')).toBeInTheDocument()
  expect(screen.getByTestId('modal')).toBeInTheDocument()
  expect(screen.getByTestId('modal-close')).toBeInTheDocument()

  fireEvent.click(screen.getByTestId('modal-close'))

  await setTimeout(() => {
    expect(screen.queryByTestId('modal-overlay')).not.toBeInTheDocument()
    expect(screen.queryByTestId('modal')).not.toBeInTheDocument()
    expect(screen.queryByTestId('modal-close')).not.toBeInTheDocument()
  })
})