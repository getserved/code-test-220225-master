import React, { FC, useMemo } from 'react';

import $ from "./EmployeeDetail.module.css"
import { Employee } from "../../core/types"
import moment from "moment"

interface EmployeeDetailProps {
    employee: Employee
}

const EmployeeDetail:FC<EmployeeDetailProps> = ({
    employee
}) => {

    const dateJoined = useMemo(() => {
        return moment(employee.dateJoined).format('DD/MM/YYYY')
    }, [employee.dateJoined])

    return (
        <div className={$.container}>
            <div className={$.avatar}><img alt="" src={'/placeholder.jpg'} data-testid="employee-avatar"/></div>
            <div className={$.info}>
                <div data-testid="employee-job-title">{employee.jobTitle}</div>
                <div data-testid="employee-age">{employee.age}</div>
                <div data-testid="employee-date-joined">{dateJoined}</div>
            </div>
            <div className={$.name}>
                <h3 className={$.fullname} data-testid="employee-name">{employee.firstName} {employee.lastName}</h3>
            </div>
            <div className={$.bio} data-testid="employee-bio">{employee.bio}</div>
        </div>
    );
}

export default EmployeeDetail;