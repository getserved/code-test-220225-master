import { screen, render } from '@testing-library/react'
import '@testing-library/jest-dom'
import EmployeeDetail from './EmployeeDetail'
import { employees } from '../../utils/TestUtils'

describe('Header component', () => {
  it('renders the header', () => {
    render(
        <EmployeeDetail employee={employees[0]}/>
    );
    expect(screen.getByTestId('employee-avatar')).toBeInTheDocument()
    expect(screen.getByTestId('employee-job-title')).toBeInTheDocument()
    expect(screen.getByTestId('employee-age')).toBeInTheDocument()
    expect(screen.getByTestId('employee-date-joined')).toBeInTheDocument()
    expect(screen.getByTestId('employee-name')).toBeInTheDocument()
    expect(screen.getByTestId('employee-bio')).toBeInTheDocument()
    expect(screen.getByTestId('employee-job-title').textContent).toBe('jobTitle')
    expect(screen.getByTestId('employee-age').textContent).toBe("18")
    expect(screen.getByTestId('employee-date-joined').textContent).toBe('01/01/1970')
    expect(screen.getByTestId('employee-name').textContent).toBe('firstName lastName')
    expect(screen.getByTestId('employee-bio').textContent).toBe('bio')
  })
});
