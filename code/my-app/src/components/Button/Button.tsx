import React, { FC, forwardRef, ForwardedRef } from "react";

import $ from "./Button.module.css";

export enum ButtonType {
  BUTTON = 'button',
  SUBMIT = 'submit',
  RESET = 'reset'
}

export enum VariantType {
  PRIMARY = 'primary',
  OUTLINE = 'outline'
}

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement>{
  children: React.ReactNode;
  onClick?: (e: React.MouseEvent) => void;
  type?: ButtonType;
  className?: string;
  variant?: VariantType;
  disabled?: boolean;
  ref?: ForwardedRef<HTMLButtonElement>;
}

// eslint-disable-next-line react/display-name
const Button: FC<ButtonProps> = forwardRef<HTMLButtonElement, ButtonProps>(({
  children,
  onClick,
  type = ButtonType.BUTTON,
  variant = VariantType.PRIMARY,
  className,
  disabled = false,
  ...rest
}, ref) => {
  return (
    <button
      ref={ref}
      disabled={disabled}
      className={`${className?className: ''} ${$.button} ${$[variant]} ${disabled?$.disabled:''}`}
      type={type}
      onClick={onClick}
      data-testid='button'
      {...rest}
    >
      {children}
    </button>
  );
});

export default Button;
