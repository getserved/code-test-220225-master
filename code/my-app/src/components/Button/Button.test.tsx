import { fireEvent, screen, render } from '@testing-library/react'
import '@testing-library/jest-dom'
import Button, { ButtonType, VariantType } from './Button'

const props = {
  type: ButtonType.BUTTON,
  variant: VariantType.OUTLINE,
  className: 'test',
}

test('Button rendering with children', () => {
  render(<Button>Test</Button>)
  expect(screen.getByText(/Test/i)).toBeInTheDocument()
})

test('Button rendering with props', () => {
  render(<Button {...props}>Test2</Button>)
  const btn = screen.getByText('Test2')
  expect(btn).toBeInTheDocument()
  expect(btn).toHaveClass('test')
  expect(btn).toHaveClass('outline')
  expect(btn).toHaveClass('button')
})

test('Button trigger click event', () => {
  const mockOnClick = jest.fn();
  render(<Button {...props} onClick={mockOnClick}>Test3</Button>)
  const btn = screen.getByText('Test3')

  fireEvent.click(btn)

  expect(mockOnClick).toBeCalledTimes(1)
})

test('Button with disabled will not trigger click event', () => {
  const mockOnClick = jest.fn();
  render(<Button {...props} disabled onClick={mockOnClick}>Test4</Button>)
  const btn = screen.getByText('Test4')

  fireEvent.click(btn)

  expect(mockOnClick).toBeCalledTimes(0)
})