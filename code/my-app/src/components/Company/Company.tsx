import React,{ useEffect, useMemo } from 'react';

import $ from "./Company.module.css"
import { useParams } from 'react-router-dom';
import SearchBar from '../SearchBar/SearchBar';
import DataTable from '../DataTable/DataTable';
import EmployeeDetail from '../EmployeeDetail/EmployeeDetail';
import useCompany from '../hooks/useCompany';
import Modal from '../Modal/Modal'
import { useAppSelector } from '../hooks';

function Company() {

    const { selectedEmployeeId, selectedEmployee } = useAppSelector(state => state.company)

    const { fetchCompany, setSelectedEmployeeId } = useCompany()

    const showDetail = useMemo(() => {
        return selectedEmployeeId !== undefined && selectedEmployeeId.length > 0
    }, [selectedEmployeeId])

    useEffect(() => {
        fetchCompany()
    }, [])

    const { page = "1" } = useParams();

    const currentPage = parseInt(page)

    const handleModalClose = () => {
        setSelectedEmployeeId('')
    }

    return (
        <div className={$.container}>
            <div className={$.content}>
                <SearchBar />
                <DataTable currentPage={currentPage}/>
                {selectedEmployeeId && selectedEmployee &&
                    <Modal isOpen={showDetail} onClose={handleModalClose}>
                        <EmployeeDetail employee={selectedEmployee}/>
                    </Modal>
                }
            </div>
        </div>
    );
}

export default Company;