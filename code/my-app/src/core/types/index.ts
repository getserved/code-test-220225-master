export type Employee = {
    id: string,
    avatar: string,
    firstName: string,
    lastName: string,
    fullName?: string,
    jobTitle: string, 
    contactNo: string,
    address: string,
    age: number,
    bio: string,
    dateJoined: string
}

export type CompanyInfo = {
    companyName: string,
    companyMotto: string,
    companyEst?: string
}

export type Company = {
    companyInfo: CompanyInfo,
    employees: Employee[]
}

export type SortPayload  = {
    field: keyof Employee;
    descending: boolean;
};