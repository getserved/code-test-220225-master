import store, { RootState } from './index'
import { reset, setQuery, setSort, sort, filter, fetchCompany, setSelectedEmployeeId } from "./CompanySlice"
import { companyRoot } from "../../utils/TestUtils"
import { waitFor } from "@testing-library/react"
import { setupStore } from "./index"

export const initState: RootState = {
    company: companyRoot
  }

describe('redux init state tests', () => {
  const state = store.getState().company
  test('Should initially set employees to empty', () => {
    expect(state.company.employees).toEqual([])
  })

  test('Should initially set companyName to ""', () => {
    expect(state.company.companyInfo.companyName).toEqual('')
  })

  test('Should initially set companyMotto to ""', () => {
    expect(state.company.companyInfo.companyMotto).toEqual('')
  })

  test('Should initially set companyEst to be undefined', () => {
    expect(state.company.companyInfo.companyEst).toEqual(undefined)
  })

  test('Should initially set query to be ""', () => {
    expect(state.query).toEqual('')
  })

  test('Should initially set selectedEmployeeId to be ""', () => {
    expect(state.selectedEmployeeId).toEqual('')
  })

  test('Should initially set selectedEmployee to be undefined', () => {
    expect(state.selectedEmployee).toEqual(undefined)
  })

  test('Should initially set sortBy to be ""', () => {
    expect(state.sortBy).toEqual('')
  })

  test('Should initially set sortDesending to be false', () => {
    expect(state.sortDescending).toEqual(false)
  })

  test('Should initially set isLoading to be true', () => {
    expect(state.isLoading).toEqual(true)
  })

  test('Should initially set errors to be true', () => {
    expect(state.errors).toEqual(undefined)
  })
})

describe('redux dispatch tests', () => {
  test('Should reset company', () => {
    store.dispatch(reset())
    const state = store.getState().company
    expect(state.company.employees).toEqual([])
  })

  test('Should set new query', () => {
    const query = { query: 'test' }
    store.dispatch(setQuery(query))
    const state = store.getState().company
    expect(state.query).toEqual(query.query)
  })

  test('Should set new sort', () => {
    const sort = {field: 'test', descending: true}
    store.dispatch(setSort(sort))
    const state = store.getState().company
    expect(state.sortBy).toEqual(sort.field)
    expect(state.sortDescending).toEqual(sort.descending)
  })

  test('Should set new selectedEmployee', () => {
    const selected = 'id'
    store.dispatch(setSelectedEmployeeId(selected)) 
    const state = store.getState().company
    expect(state.selectedEmployeeId).toEqual(selected)
    expect(state.selectedEmployee).toEqual(state.company.employees[0])
  })

  test('Should fetch new data', async () => {
    store.dispatch(fetchCompany()) 
    const state = store.getState().company
    await setTimeout(()=> {
      expect(state.company.employees.length).toBeGreaterThan(1)
      expect(state.filteredEmployees.length).toBeGreaterThan(1)
    })    
  })

  test('Should sort by name ascending', async () => {  
    const queryStates = {
      company: {
        ...initState.company,
        filteredEmployees: [
          initState.company.filteredEmployees[0],
          {
            ...initState.company.filteredEmployees[0],
            firstName: 'abc'
          }
        ],
        sortBy: 'firstName',
        sortDescending: true
      }
    }
    const store = setupStore(queryStates)

    await store.dispatch(sort({}))

    await setTimeout(()=> {
      const state = store.getState().company
      expect(state.filteredEmployees[0].firstName).toBe('firstName')
    })    
    
  })

  test('Should filter by query', async () => {  
    const queryStates = {
      company: {
        ...initState.company,
        query: 'id'
      }
    }
    const store = setupStore(queryStates)

    await store.dispatch(filter({}))
    
    await waitFor(()=> {
      const state = store.getState().company
      expect(state.filteredEmployees[0].id).toBe('id')
    })    
    
  })
})