import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'
import { BASE_URL } from "../../utils/constants"
import type { Company, Employee } from "../types"
import _ from "lodash"

const NORMAL_ERROR = "There's an error when matching jobs"

interface CompanyState {
  company: Company,
  filteredEmployees: Employee[],
  selectedEmployeeId: string,
  selectedEmployee: Employee | undefined,
  query: string,
  sortBy: string,
  sortDescending: boolean,
  isLoading: boolean,
  errors: string | undefined
}

// Init states for Job
const initialState: CompanyState = {
  company: {
    companyInfo: {
      companyName: '',
      companyMotto: '',
      companyEst: undefined
    },
    employees: []
  },
  filteredEmployees: [],
  selectedEmployeeId: '',
  selectedEmployee: undefined,
  query: '',
  sortBy: '',
  sortDescending: false,
  isLoading: true,
  errors: undefined
};

// CreateAsyncThunk for async function to fetch data
export const fetchCompany = createAsyncThunk("company/fetch", async (_:void, { rejectWithValue }) => {
  try{
    const res = await axios(
      `${BASE_URL}/data`,
      {
        method: "GET",
        headers: {
          'Access-Control-Allow-Origin': BASE_URL,
          'Content-Type': 'application/json'
        },
      }
    )
    const data = await res.data;
    
    if(res.status >= 400 && res.status < 600) {
      return rejectWithValue(data.errormessage || NORMAL_ERROR)
    }
    
    return data
  // error handling for exceptions
  } catch (e: any) {
    return rejectWithValue(e.message || NORMAL_ERROR)
  }
   
})

// Create Slice for Company
export const companySlice = createSlice({
  name: 'company',
  initialState,
  reducers: {
    // reset
    reset: () => initialState,

    // action to set new Company
    setSort: (state, action) => {
      const {field, descending} = action.payload
      state.sortBy = field
      state.sortDescending = descending
    },

    // set query
    setQuery: (state, action) => {
      const {query} = action.payload
      state.query = query
    },

    // set selectedEmployeeId and selectedEmployee
    setSelectedEmployeeId: (state, action) => {
      const id = action.payload
      state.selectedEmployeeId = id

      state.selectedEmployee = state.company.employees.filter(employee => employee.id === id)[0]
    },

    // sorting by sortBy and sortDescending
    sort: (state, action) => {
      const field = state.sortBy as keyof Employee
      const descending = state.sortDescending

      state.filteredEmployees = state.company.employees.sort((a, b) => {
        const aVal = a[field]
        const bVal = b[field]

        if(aVal && bVal){
          if(_.isString(aVal) && _.isString(bVal)) {
            return descending? (bVal.localeCompare(aVal)) : (aVal.localeCompare(bVal))
          } else if ((_.isNumber(aVal) && _.isNumber(bVal))) {
            return descending? (bVal - aVal):(aVal - bVal)
          }
        }
        return 0
      })
    },

    // filtering by query
    filter: (state, action) => {
      const query = state.query

      state.filteredEmployees = state.company.employees.filter(employee => {
        const db = Object.values(employee).join('').toLowerCase()
        return db.includes(query?.toLowerCase())
      })
    }
    
  },
  // All extra reducers lie in here
  extraReducers: (builder) => {
    builder
      .addCase(fetchCompany.pending, (state, action) => {
        state.isLoading = true
      })
      .addCase(fetchCompany.fulfilled, (state, action) => {
        const data = action.payload
        // set company as origional data
        state.company = data
        // init filteredEmployees and transfer `firstName lastName` to fullName
        state.filteredEmployees = state.company.employees.map(employee => {
          employee['fullName'] = `${employee.firstName} ${employee.lastName}`
          return employee
        })
        state.company.employees = state.filteredEmployees
        state.isLoading = false
      })
      .addCase(fetchCompany.rejected, (state, action) => {
        if (action.payload) {
          // Being that we passed in ValidationErrors to rejectType in `createAsyncThunk`, the payload will be available here.
          state.errors = (action.payload as string)
        } else {
          state.errors = action.error.message
        }
        state.isLoading = false
      })
  }
})

export const { setSort, setQuery, setSelectedEmployeeId, reset, sort, filter } = companySlice.actions

export default companySlice.reducer