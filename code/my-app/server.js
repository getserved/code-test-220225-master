/* eslint-disable @typescript-eslint/no-var-requires */
// eslint-disable-next-line @typescript-eslint/no-var-requires
const express = require( "express" );
const cors = require("cors")
const app = express();
const port = 8080; // default port to listen
app.use(cors())

// define a route handler for the default home page
app.get( "/data", ( req, res, next ) => {
    try{
        const data = require('./sample-data.json');
        if(data)
            res.json(data)
    }catch(e) {
        e.status = 500
        next(e)
    }
} );

app.use((err, req, res, next) => {
    res.status(err.status || 500).json({
      message: err.message,
      error: err,
    });
  });

// start the Express server
app.listen( port, () => {
    console.log( `server started at http://localhost:${ port }` );
} );